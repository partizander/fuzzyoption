
class AlphaCut {
private:
	double alphaCut[2];
public:
	AlphaCut(){}
	AlphaCut(double a, double b, double c, double alpha);
	AlphaCut(double left, double right);
	AlphaCut(double crispNumber);
	AlphaCut(const AlphaCut& A);
	
	double getLeft() { return alphaCut[0]; }
	double getRight() { return alphaCut[1]; }

	~AlphaCut(){}

	AlphaCut operator-(AlphaCut A);
	AlphaCut operator+(AlphaCut A);
	AlphaCut operator=(AlphaCut A);
	AlphaCut operator*(AlphaCut A);
	AlphaCut operator^(int A) const;
	AlphaCut operator/(AlphaCut A);
	AlphaCut& operator+=(AlphaCut A);
};
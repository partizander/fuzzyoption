#include "AlphaCut.h"

AlphaCut::AlphaCut(double a, double b, double c, double alpha) {
	alphaCut[0] = a + (b - a) * alpha;
	alphaCut[1] = c - (c - b) * alpha;
}

AlphaCut::AlphaCut(double left, double right) {
	alphaCut[0] = left;
	alphaCut[1] = right;
}

AlphaCut::AlphaCut(double crispNumber) {
	alphaCut[0] = alphaCut[1] = crispNumber;
}

AlphaCut::AlphaCut (const AlphaCut& A) {
	alphaCut[0] = A.alphaCut[0];
	alphaCut[1] = A.alphaCut[1];
}

AlphaCut AlphaCut::operator-(AlphaCut A) {
	return AlphaCut(alphaCut[0] - A.alphaCut[1], alphaCut[1] - A.alphaCut[0]);
}

AlphaCut AlphaCut::operator+(AlphaCut A) {
	return AlphaCut(alphaCut[0] + A.alphaCut[0], alphaCut[1] + A.alphaCut[1]);
}

AlphaCut AlphaCut::operator=(AlphaCut A) {
	alphaCut[0] = A.alphaCut[0];
	alphaCut[1] = A.alphaCut[1];
	return *this;
}

AlphaCut AlphaCut::operator*(AlphaCut A) {
	double a = (alphaCut[0] * A.alphaCut[0] <= alphaCut[1] * A.alphaCut[1]) ?
		alphaCut[0] * A.alphaCut[0] : alphaCut[1] * A.alphaCut[1];
	double b = (alphaCut[0] * A.alphaCut[0] >= alphaCut[1] * A.alphaCut[1]) ?
		alphaCut[0] * A.alphaCut[0] : alphaCut[1] * A.alphaCut[1];
	return AlphaCut(a,b);
}

AlphaCut AlphaCut::operator^(int degree) const{
	if (degree == 0) return AlphaCut(1);
	else if (degree == 1) return (*this);
	else {
		AlphaCut A = *this;
		for (int i = 1; i < degree; ++i) {
			A = A * (*this);
		}
		return A;
	}
}

AlphaCut AlphaCut::operator/(AlphaCut A) {
	return AlphaCut(alphaCut[0] / A.alphaCut[1], alphaCut[1] / A.alphaCut[0]);
}

AlphaCut& AlphaCut::operator+=(AlphaCut A) {
	alphaCut[0] += A.alphaCut[0];
	alphaCut[1] += A.alphaCut[1];
	return *this;
}
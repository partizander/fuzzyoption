#include "AlphaCut.h"
#include <iostream>
#include <cmath>

void print(AlphaCut A);
AlphaCut max(double zero, AlphaCut A);
AlphaCut exponential(AlphaCut A);
int min(int a, int b);
int binomialCoeff(int n, int k);

int main() {
	int step = 2;
	double time = 0.5;
	AlphaCut result(0,0);
	for (double alpha = 0; alpha <= 1; alpha += 0.25) {
		
		AlphaCut p(57, 60, 63, alpha), a(0.04, 0.05, 0.06, alpha),
			S(60, 62, 64, alpha), r(0.05, 0.06, 0.07, alpha), one(1), U(one + a), V(one - a), T(time/step), TT(time), minusOne(-1);
		AlphaCut theta = (exponential(r*T) - V) / (U - V);
		for (int i = 0; i <= step; ++i) {
			
			AlphaCut C = max(0, p * (U ^ (step - i)) * (V ^ i) - S);
			
			AlphaCut binomial(binomialCoeff(step, i));
			//print(C);
			
			//print(p * (U ^ (step - i)) * (V ^ i));
			
			result += binomial * (theta ^ (step - i)) * ((one - theta) ^ i) * C;
		}
		result = result * exponential(minusOne * r * TT);
		print(result);
		std::cout << " alpha = " << alpha << std::endl;
		result = 0;
	}

	system("pause");
	return 0;
}

void print(AlphaCut A) {
	printf("[%.2f %.2f] ", A.getLeft(), A.getRight());
}

AlphaCut max(double zero, AlphaCut A) {
	double a = (zero > A.getLeft()) ? zero : A.getLeft();
	double b = (zero > A.getRight()) ? zero : A.getRight();
	return AlphaCut(a,b);
}

AlphaCut exponential(AlphaCut A) {
	return AlphaCut(pow(exp(1), A.getLeft()), pow(exp(1), A.getRight()));
}

int min(int a, int b) {
	return (a<b) ? a : b;
}

int binomialCoeff(int n, int k) {
	int* C = (int*)calloc(k + 1, sizeof(int));
	int i, j, res;

	C[0] = 1;

	for (i = 1; i <= n; i++)
	{
		for (j = min(i, k); j > 0; j--)
			C[j] = C[j] + C[j - 1];
	}

	res = C[k];

	free(C);

	return res;
}